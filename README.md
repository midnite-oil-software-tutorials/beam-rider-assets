# Beam Rider Assets

# Sounds
https://freesound.org/people/OwlStorm/sounds/404796/
https://freesound.org/people/Soundholder/sounds/425335/
https://freesound.org/people/timgormly/sounds/170144/

Explosion animations
https://free-game-assets.itch.io/free-animated-explosion-sprite-pack

Game Creator's Pack
https://jonathan-so.itch.io/creatorpack

Arcade Font
https://www.1001freefonts.com/arcade.font